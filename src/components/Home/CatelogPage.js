import _ from 'lodash';
import React, {PropTypes, Component} from 'react';
import ReactNative, {View, Text} from 'react-native';

import RefreshableListView from '../Common/RefreshableListView';
import CatelogItem from './CatelogItem';
import HeaderBanner from './HeaderBanner';

class CatelogPage extends Component {
    render() {
        const { banner } = this.props;
        const attr = {
            listView: {
                ref: 'listView',
                rowView: (row) => <CatelogItem data={row}/>,
                onFetch: this.onFetch,
                pageSize: 4,
                renderSeparator: () => <View style={{height: 1,backgroundColor: '#CCC'}} />,
                optionText: {
                    loadMore: '點擊載入...',
                    allLoaded: '已經全部載入',
                    empty: '尚未有資料',
                    refresh: '重新整理',
                },
                headerView: () => <HeaderBanner data={banner}/>,
            }
        }
        return <RefreshableListView {...attr.listView}/>
    }

    onFetch = (page, callback) => {
        const { data } = this.props;
        callback(data, { allLoaded: true });
    }
}

CatelogPage.propTypes = {
    data: PropTypes.array,
};

export default CatelogPage;
