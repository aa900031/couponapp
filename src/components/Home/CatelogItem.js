import React, {PropTypes, Component} from 'react';
import ReactNative, {View, Text, StyleSheet, Dimensions, Image, TouchableHighlight} from 'react-native';
import { Actions } from 'react-native-router-flux';

class CatelogItem extends Component {
    render() {
        const { data } = this.props

        return (
            <TouchableHighlight onPress={() => this.onPressAll(data.key)}>
            <View style={styles.container}>
                <Image style={styles.image} source={require('../../assets/images/test1.png')}/>
                <View style={styles.rightContainer}>
                    <Text style={styles.title}>{data.title}</Text>
                    <Text style={styles.des}>{data.des}</Text>
                </View>
            </View>
            </TouchableHighlight>
        );
    }

    onPressAll = (key) => {
        Actions.shop({ _key: key });
    }
}

CatelogItem.propTypes = {
    data: PropTypes.object,
};

const styles = StyleSheet.create({
    container: {
        height: (Dimensions.get('window').width / 2 ) - 60,
        flexDirection: 'row',
        backgroundColor: '#FFF'
    },
    rightContainer: {
        flex: 1,
        flexDirection: 'column',
    },
    title: {
        fontSize: 18,
        margin: 7,
    },
    des: {
        fontSize: 14,
        color: 'gray',
        marginLeft: 7,
        marginRight: 7,
        marginBottom: 7,
    },
    image: {
        height: (Dimensions.get('window').width / 2 ) - 60,
        width: (Dimensions.get('window').width / 2 ) - 60,
    }
})

export default CatelogItem;
