import React, { Component, PropTypes } from 'react'
import {
    StyleSheet,
    Text,
    View,
    Platform,
    Dimensions,
    RefreshControl,
    ListView,
    ActivityIndicator,
    TouchableHighlight,
} from 'react-native'
import _ from 'lodash'

const PAGINATION_STATUS_FIRSTLOAD = 1;
const PAGINATION_STATUS_FETCHING = 2;
const PAGINATION_STATUS_WAITING = 3;
const PAGINATION_STATUS_ALL_LOADED = 4;

class RefreshableListView extends Component {
    static defaultProps = {
        customStyles: {},
        initialListSize: 10,
        firstLoader: true,
        pagination: true,
        refreshable: true,
        refreshableColors: undefined,
        refreshableProgressBackgroundColor: undefined,
        refreshableSize: undefined,
        refreshableTitle: undefined,
        refreshableTintColor: undefined,
        renderRefreshControl: null,
        headerView: null,
        sectionHeaderView: null,
        scrollEnabled: true,
        withSections: false,
        paginationFetchingView: null,
        paginationAllLoadedView: null,
        paginationWaitingView: null,
        emptyView: null,
        renderSeparator: null,
        onFetch: (page, callback, options) => { callback([]); },
        optionText: {},
        delayTime: 300,
    };
    static propTypes = {
        customStyles: PropTypes.object,
        initialListSize: PropTypes.number,
        firstLoader: PropTypes.bool,
        pagination: PropTypes.bool,
        refreshable: PropTypes.bool,
        refreshableColors: PropTypes.array,
        refreshableProgressBackgroundColor: PropTypes.string,
        refreshableSize: PropTypes.string,
        refreshableTitle: PropTypes.string,
        refreshableTintColor: PropTypes.string,
        renderRefreshControl: PropTypes.func,
        headerView: PropTypes.func,
        sectionHeaderView: PropTypes.func,
        scrollEnabled: PropTypes.bool,
        withSections: PropTypes.bool,
        onFetch: PropTypes.func,
        paginationFetchingView: PropTypes.func,
        paginationAllLoadedView: PropTypes.func,
        paginationWaitingView: PropTypes.func,
        emptyView: PropTypes.func,
        renderSeparator: PropTypes.func,
        optionText: PropTypes.object,
        delayTime: PropTypes.number,
    };

    constructor(props) {
        super(props);

        this._setPage(1);
        this._setRows([]);

        let ds = null;
        let withSections = this.props.withSections === true;

        if (withSections) {
            ds = new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
                sectionHeaderHasChanged: (section1, section2) => section1 !== section2,
            });
        } else {
            ds = new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            });
        }

        this.state = {
            dataSource: withSections ? ds.cloneWithRowsAndSections(this._getRows()) : ds.cloneWithRows(this._getRows()),
            isRefreshing: false,
            paginationStatus: PAGINATION_STATUS_FIRSTLOAD,
        }
    }
    componentDidMount() {
        this._mounted = true;
        _.delay(() => {
            this.props.onFetch(this._getPage(), this._postRefresh, {firstLoad: true});
        }, this.props.delayTime);
    }
    componentWillUnmount() {
        this._mounted = false;
    }
    render() {
        if (this.state.paginationStatus === PAGINATION_STATUS_FIRSTLOAD && this.props.firstLoader === true) {
            return (
                <View style={[styles.container, styles.defaultView, this.props.customStyles.defaultView]}>
                    <ActivityIndicator animating={true} size="large" />
                </View>
            );
        } else if (this._getRows().length === 0) {
            return this.emptyView(this._onRefresh);
        } else {
            return (
                <ListView
                    ref="listview"
                    dataSource={this.state.dataSource}
                    renderRow={this.props.rowView}
                    renderSectionHeader={this.props.sectionHeaderView}
                    renderHeader={this.headerView}
                    renderFooter={this._renderPaginationView}
                    renderSeparator={this.renderSeparator}
                    automaticallyAdjustContentInsets={false}
                    scrollEnabled={this.props.scrollEnabled}
                    canCancelContentTouches={true}
                    refreshControl={this.props.refreshable === true ? this.renderRefreshControl() : null}

                    {...this.props}

                    style={[styles.container, this.props.style]}
                />
            );
        }

    }
    // Other render
    paginationFetchingView = () => {
        if (this.props.paginationFetchingView) {
            return this.props.paginationFetchingView();
        }
        return (
            <View style={[styles.paginationView, this.props.customStyles.paginationView]}>
                <ActivityIndicator animating={true} size="small" />
            </View>
        );
    }
    paginationAllLoadedView = () => {
        if (this.props.paginationAllLoadedView) {
            return this.props.paginationAllLoadedView();
        }
        let label = this.props.optionText.allLoaded || 'All loaded';

        return (
            <View style={[styles.paginationView, this.props.customStyles.paginationView]}>
                <Text style={[styles.actionsLabel, this.props.customStyles.actionsLabel]}>
                    {label}
                </Text>
            </View>
        );
    }
    paginationWaitingView = (paginateCallback) => {
        if (this.props.paginationWaitingView) {
            return this.props.paginationWaitingView(paginateCallback);
        }
        let label = this.props.optionText.loadMore || 'Load more';

        return (
            <TouchableHighlight style={[styles.paginationView, this.props.customStyles.paginationView]}
                underlayColor='#c8c7cc'
                onPress={paginateCallback}
                >
                <Text style={[styles.actionsLabel, this.props.customStyles.actionsLabel]}>
                    {label}
                </Text>
            </TouchableHighlight>
        );
    }
    headerView = () => {
        if (this.state.paginationStatus === PAGINATION_STATUS_FIRSTLOAD || !this.props.headerView){
            return null;
        }
        return this.props.headerView();
    }
    emptyView = (refreshCallback) => {
        if (this.props.emptyView) {
            return this.props.emptyView(refreshCallback);
        }
        let label = this.props.optionText.empty || 'Sorry, there is no content to display';
        let label2 = this.props.optionText.refresh || 'Refresh';

        return (
            <View style={[styles.container, styles.defaultView, this.props.customStyles.defaultView]}>
                <Text style={[styles.defaultViewTitle, this.props.customStyles.defaultViewTitle]}>
                    {label}
                </Text>

                <TouchableHighlight underlayColor='#c8c7cc' onPress={refreshCallback}>
                    <Text>{label2}</Text>
                </TouchableHighlight>
            </View>
        );
    }
    renderSeparator = () => {
        if (this.props.renderSeparator) {
            return this.props.renderSeparator();
        }
        return (
            <View style={[styles.separator, this.props.customStyles.separator]} />
        );
    }
    _renderPaginationView = () => {
        /* 先保留
        if ((this.state.paginationStatus === PAGINATION_STATUS_FETCHING && this.props.pagination === true) || (this.state.paginationStatus === PAGINATION_STATUS_FIRSTLOAD && this.props.firstLoader === true)) {
            return this.paginationFetchingView();
        } else if (this.state.paginationStatus === PAGINATION_STATUS_WAITING && this.props.pagination === true && (this.props.withSections === true || this._getRows().length > 0)) {
            return this.paginationWaitingView(this._onPaginate);
        } else if (this.state.paginationStatus === PAGINATION_STATUS_ALL_LOADED && this.props.pagination === true) {
            return (this._getPage() > 1) ? this.paginationAllLoadedView() : null
        } else if (this._getRows().length === 0) {
            return this.emptyView(this._onRefresh);
        } else {
            return null;
        }
        */
        if ((this.state.paginationStatus === PAGINATION_STATUS_FETCHING && this.props.pagination === true)) {
            return this.paginationFetchingView();
        } else if (this.state.paginationStatus === PAGINATION_STATUS_WAITING && this.props.pagination === true && (this.props.withSections === true || this._getRows().length > 0)) {
            return this.paginationWaitingView(this._onPaginate);
        } else if (this.state.paginationStatus === PAGINATION_STATUS_ALL_LOADED && this.props.pagination === true) {
            return (this._getPage() > 1) ? this.paginationAllLoadedView() : null
        } else {
            return null;
        }
    }

    renderRefreshControl = () => {
        if (this.props.renderRefreshControl) {
            return this.props.renderRefreshControl({ onRefresh: this._onRefresh });
        }
        return (
            <RefreshControl
                onRefresh={this._onRefresh}
                refreshing={this.state.isRefreshing}
                colors={this.props.refreshableColors}
                progressBackgroundColor={this.props.refreshableProgressBackgroundColor}
                size={this.props.refreshableSize}
                tintColor={this.props.refreshableTintColor}
                title={this.props.refreshableTitle}
                />
        );
    }

    // Custom functions
    _setPage = (page) => { this._page = page; }
    _getPage = () => { return this._page; }
    _setRows = (rows) => { this._rows = rows; }
    _getRows = () => { return this._rows; }

    setNativeProps = (props) => {
        this.refs.listview.setNativeProps(props);
    }

    _refresh = () => {
        this._onRefresh({external: true});
    }

    _onRefresh = (options = {}) => {
        if (this._mounted) {
            this.setState({
                isRefreshing: true,
            });
            this._setPage(1);
            _.delay(() => {
                this.props.onFetch(this._getPage(), this._postRefresh, options);
            }, this.props.delayTime);
        }
    }

    _postRefresh = (rows = [], options = {}) => {
        if (this._mounted) {
            this._updateRows(rows, options);
        }
    }

    _onPaginate = () => {
        if(this.state.paginationStatus === PAGINATION_STATUS_ALL_LOADED){
            return null
        }else {
            this.setState({
                paginationStatus: PAGINATION_STATUS_FETCHING,
            });
            _.delay(() => {
                this.props.onFetch(this._getPage() + 1, this._postPaginate, {});
            }, this.props.delayTime);
        }
    }

    _postPaginate = (rows = [], options = {}) => {
        this._setPage(this._getPage() + 1);
        var mergedRows = null;
        if (this.props.withSections === true) {
            mergedRows = MergeRecursive(this._getRows(), rows);
        } else {
            mergedRows = this._getRows().concat(rows);
        }
        this._updateRows(mergedRows, options);
    }

    _updateRows = (rows = [], options = {}) => {
        if (rows !== null) {
            this._setRows(rows);
            if (this.props.withSections === true) {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRowsAndSections(rows),
                    isRefreshing: false,
                    paginationStatus: (options.allLoaded === true ? PAGINATION_STATUS_ALL_LOADED : PAGINATION_STATUS_WAITING),
                });
            } else {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(rows),
                    isRefreshing: false,
                    paginationStatus: (options.allLoaded === true ? PAGINATION_STATUS_ALL_LOADED : PAGINATION_STATUS_WAITING),
                });
            }
        } else {
            this.setState({
                isRefreshing: false,
                paginationStatus: (options.allLoaded === true ? PAGINATION_STATUS_ALL_LOADED : PAGINATION_STATUS_WAITING),
            });
        }
    }
}

// export default class extends Component {
//     render() {
//         return (
//             <RefreshableListView ref="listView"
//                 rowView={this.props.renderRow}
//                 onFetch={this.props.onRefresh}
//                 pageSize={4}
//                 renderSeparator={() => <View style={{height: 1,backgroundColor: '#CCC'}} />}
//                 optionText={{
//                     loadMore: Language.Translate('click_load'),
//                     allLoaded: Language.Translate('all_load'),
//                     empty: Language.Translate('empty'),
//                     refresh: Language.Translate('refresh'),
//                 }}
//                 {...this.props}
//             />
//         )
//     }
//     //Public
//     refreshList = () => {
//         this.refs.listView._refresh();
//     }
// }

const styles = {
    container: {
        backgroundColor: '#FFF',
    },
    separator: {
        height: 1,
        backgroundColor: '#CCC'
    },
    actionsLabel: {
        fontSize: 16,
        color: 'gray',
    },
    paginationView: {
        height: 50,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
        paddingBottom: 20,
        backgroundColor: '#FFF',
    },
    defaultView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
    },
    defaultViewTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 15,
    },
};

export default RefreshableListView;
