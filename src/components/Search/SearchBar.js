import _ from 'lodash'
import React, {PropTypes, Component} from 'react';
import ReactNative, {View, TextInput, StyleSheet, PixelRatio, Platform} from 'react-native';
import { Actions } from 'react-native-router-flux'
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard'
import Button from 'react-native-button'
import Icon from 'react-native-vector-icons/MaterialIcons'

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    render() {
        return (
            <View style={styles.toolbar} >
                <View style={styles.searchbar}>
                    <Button onPress={this.toggleDrawer}>
                        <Icon style={styles.toolbarIcon} name="menu" size={25} color={'#000000'}/>
                    </Button>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <TextInput
                            ref="searchInput"
                            style={styles.searchTextInput}
                            underlineColorAndroid={'#00000000'}
                            onChangeText={this.onChangeText}
                            onSubmitEditing={this.onSearch}
                            placeholder="搜尋"
                        />
                    </View>
                    <Button onPress={this.clearText}>
                        <Icon style={styles.toolbarIcon} name="clear" size={25} color={'#000000'}/>
                    </Button>
                </View>
            </View>
        );
    }

    onChangeText = (keyword) => {
        this.setState({ keyword: keyword });
    }

    onSearch = () => {
        let keyword = _.trim(this.state.keyword);
        if (keyword === '') {
            this.refs.searchInput.focus()
        } else {
            Actions.searchResult({
                keyword: { text: keyword }
            })
        }
    }

    clearText = () => {
        this.refs.searchInput.clear();
        dismissKeyboard();
    }

    toggleDrawer = () => {
        Actions.refresh({key: 'drawer', open: true});
    }
}

SearchBar.propTypes = {
};

const styles = StyleSheet.create({
    toolbar: {
        alignItems: 'center',
        flexDirection: 'row',
        height: 56,
        padding: 5,
        backgroundColor: '#FFFFFF',
        marginTop: Platform.OS === 'ios' ? 20 : 0,
    },
    searchbar: {
        flex: 1,
        flexDirection: "row",
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#c4c4c4',
        alignItems: 'center',
        backgroundColor: '#F6F6F6',
    },
    toolbarIcon:{
        marginLeft: 15,
        marginRight: 15,
    },
    searchTextInput:{
        flex: 1,
        fontSize: 20,
        paddingTop: 2,
        paddingBottom: 2,
        textAlignVertical: 'center',
    },
});

export default SearchBar;
