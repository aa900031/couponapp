import _ from 'lodash';
import React, {PropTypes, Component} from 'react';
import ReactNative, {View, Text, TouchableOpacity, StyleSheet, ScrollView} from 'react-native';


class Categories extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView style={styles.container}>
            {
                _.map(data, (item, key) => {
                    return <View style={styles.containerCatgory}>
                        <Text style={styles.title}>{item.name}</Text>
                        <View style={styles.containerTag}>
                        {
                            _.map(item.subCategory, (_item, _key) => {
                                return <TouchableOpacity
                                    style={styles.btnCategory}
                                    onPress={ () => console.log('test')} >
                                    <Text style={styles.textCategory}>{_item.name}</Text>
                                </TouchableOpacity>
                            })
                        }
                        </View>
                    </View>
                })
            }
            </ScrollView>
        );
    }
}

Categories.propTypes = {
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    containerCatgory: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
    },
    containerTag: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        backgroundColor: '#FFFFFF',
    },
    title: {
        margin: 10,
        fontSize: 17,
    },
    btnCategory: {
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#CCC',
        height: 42,
        margin: 5,
    },
    textCategory: {
        margin: 10,
        fontSize: 15,
        alignSelf: 'center',
        justifyContent: 'center',
    },
});

const data = [
    {
        name: '食',
        subCategory: [
            { name: 'test1', key: '' },
            { name: 'test2', key: '' },
            { name: 'test3', key: '' },
            { name: 'test4', key: '' },
            { name: 'test5', key: '' },
            { name: 'test6', key: '' },
            { name: 'test7', key: '' },
        ]
    },
    {
        name: '衣',
        subCategory: [
            { name: 'test1', key: '' },
            { name: 'test2', key: '' },
            { name: 'test3', key: '' },
            { name: 'test4', key: '' },
            { name: 'test5', key: '' },
            { name: 'test6', key: '' },
            { name: 'test7', key: '' },
        ]
    },
    {
        name: '住',
        subCategory: [
            { name: 'test1', key: '' },
            { name: 'test2', key: '' },
            { name: 'test3', key: '' },
            { name: 'test4', key: '' },
            { name: 'test5', key: '' },
            { name: 'test6', key: '' },
            { name: 'test7', key: '' },
        ]
    },
    {
        name: '行',
        subCategory: [
            { name: 'test1', key: '' },
            { name: 'test2', key: '' },
            { name: 'test3', key: '' },
            { name: 'test4', key: '' },
            { name: 'test5', key: '' },
            { name: 'test6', key: '' },
            { name: 'test7', key: '' },
        ]
    },
    {
        name: '育',
        subCategory: [
            { name: 'test1', key: '' },
            { name: 'test2', key: '' },
            { name: 'test3', key: '' },
            { name: 'test4', key: '' },
            { name: 'test5', key: '' },
            { name: 'test6', key: '' },
            { name: 'test7', key: '' },
        ]
    },
    {
        name: '樂',
        subCategory: [
            { name: 'test1', key: '' },
            { name: 'test2', key: '' },
            { name: 'test3', key: '' },
            { name: 'test4', key: '' },
            { name: 'test5', key: '' },
            { name: 'test6', key: '' },
            { name: 'test7', key: '' },
        ]
    },
]

export default Categories;
