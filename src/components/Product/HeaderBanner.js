import React, {Component, PropTypes} from 'react';
import ReactNative, {View, Text} from 'react-native';
import Swiper from 'react-native-swiper';

class HeaderBanner extends Component {
    render() {
        const { data } = this.props;
        const attr = {
            swiper: {
                autoplay: true,
                height: 170,
            },
            slideView: {
                style: {
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#9DD6EB',
                }
            },
            slideText: {
                style: {
                    color: '#fff',
                    fontSize: 30,
                    fontWeight: 'bold',
                }
            }
        }

        return (
            <Swiper {...attr.swiper} >
                {
                    data.map((item, key) => {return (
                        <View {...attr.slideView}>
                            <Text key={key} {...attr.slideText}>{item.title}</Text>
                        </View>
                    )})
                }
            </Swiper>
        );
    }
}
HeaderBanner.propTypes = {
    data: PropTypes.array,
};

HeaderBanner.defaultProps = {
    data: [],
};

export default HeaderBanner;
