import React, {PropTypes, Component} from 'react';
import ReactNative, {View, Text, Image, ScrollView, StyleSheet} from 'react-native';

class Main extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <ScrollView style={styles.container}>
                <Image style={styles.imgHeader} source={{uri: 'http://placehold.it/350x150?text=ShopHeader'}} />
        </ScrollView>
    }
}

Main.propTypes = {
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    imgHeader: {
        // flex: 1,
        height: 210,
    },
})

export default Main;
