import React, { PropTypes, Component } from 'react';
import ReactNative, { View, Text, Image, ScrollView, StyleSheet, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

class Products extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <ScrollView style={styles.container}>
            <TouchableHighlight onPress={() => this.onPressProduct()}>
                <Image style={styles.imgHeader} source={{uri: 'http://placehold.it/350x150?text=Product'}} />
            </TouchableHighlight>
            <TouchableHighlight onPress={() => this.onPressProduct()}>
                <Image style={styles.imgHeader} source={{uri: 'http://placehold.it/350x150?text=Product'}} />
            </TouchableHighlight>
        </ScrollView>
    }

    onPressProduct = () => {
        Actions.product();
    }
}

Products.propTypes = {
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    imgHeader: {
        // flex: 1,
        height: 210,
    },
})

export default Products;
