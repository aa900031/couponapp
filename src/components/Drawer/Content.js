import _ from 'lodash';
import React, { PropTypes, Component, } from 'react';
import ReactNative, { View, Text, Image, StyleSheet, ScrollView, } from 'react-native';
import {Actions} from 'react-native-router-flux';
import Button from 'react-native-button';
import {
    Cell,
    CustomCell,
    Section,
    TableView,
} from 'react-native-tableview-simple';

class Content extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <Image style={styles.headerImage} source={require('../../assets/images/header.png')}/>
                <TableView >
                    <Section>
                        <Cell cellStyle="Basic" title="首頁" accessory="DisclosureIndicator"
                            onPress={() => this.onPressAction(Actions.home)}/>
                        <Cell cellStyle="Basic" title="搜尋" accessory="DisclosureIndicator"
                            onPress={() => this.onPressAction(Actions.search)}/>
                        <Cell cellStyle="Basic" title="Test" accessory="DisclosureIndicator"
                            onPress={() => this.onPressAction(Actions.test)}/>
                    </Section>
                </TableView>
            </ScrollView>
        );
    }

    onPressAction = (action) => {
        Actions.refresh({
            key: this.props.drawKey,
            open: false
        }, _.delay(() => action(), 300));
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#EFEFF4',
    },
    headerImage: {
        height: 180,
        width: 280,
    },
})

Content.propTypes = {
};

export default Content;
