import _ from 'lodash';
import React, {Component} from 'react';
import ReactNative, {View, Text} from 'react-native';
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';

import CatelogPage from '../components/Home/CatelogPage.js'

class Home extends Component {
    render() {
        const attr = {
            rootView: {
                style: {
                    flex: 1,
                }
            },
            scrollTabView: {
                style: {
                    marginTop: 60,
                },
                renderTabBar: () => <ScrollableTabBar {...attr.tabBar}/>,
            },
            tabBar: {
                tabBarPosition: 'overlayTop',
            }
        }

        return (
            <View {...attr.rootView}>
                <ScrollableTabView {...attr.scrollTabView}>
                {
                    _.map(fackData.catelogPage, (item, key) => <CatelogPage key={key} tabLabel={item.name} banner={item.banner} data={item.items}/>)
                }
                </ScrollableTabView>
            </View>
        );
    }
}

const fackData = {
    catelogPage: {
        food: {
            name: '食',
            banner: [
                { title: 'Test' },
                { title: 'Test2' }
            ],
            items: [
                { image: '../../assets/images/test1.png', title: '優惠卷1', des: '這是優惠卷～～～～', key: 1 },
                { image: '../../assets/images/test2.png', title: '優惠卷2', des: '這是優惠卷～～～～', key: 2 },
                { image: '../../assets/images/test3.png', title: '優惠卷3', des: '這是優惠卷～～～～', key: 3 },
                { image: '../../assets/images/test4.png', title: '優惠卷4', des: '這是優惠卷～～～～', key: 4 },
                { image: '../../assets/images/test5.png', title: '優惠卷5', des: '這是優惠卷～～～～', key: 5 },
                { image: '../../assets/images/test6.png', title: '優惠卷6', des: '這是優惠卷～～～～', key: 6 },
                { image: '../../assets/images/test7.png', title: '優惠卷7', des: '這是優惠卷～～～～', key: 7 },
                { image: '../../assets/images/test8.png', title: '優惠卷8', des: '這是優惠卷～～～～', key: 8 },
                { image: '../../assets/images/test9.png', title: '優惠卷9', des: '這是優惠卷～～～～', key: 9 },
                { image: '../../assets/images/test10.png', title: '優惠卷10', des: '這是優惠卷～～～～', key: 10 }
            ],
        },
        clothing: {
            name: '衣',
            banner: [
                { title: 'Test' },
                { title: 'Test2' }
            ],
            items: [
                { image: '../../assets/images/test1.png', title: '優惠卷1', des: '這是優惠卷～～～～', key: 1 },
                { image: '../../assets/images/test2.png', title: '優惠卷2', des: '這是優惠卷～～～～', key: 2 },
                { image: '../../assets/images/test3.png', title: '優惠卷3', des: '這是優惠卷～～～～', key: 3 },
                { image: '../../assets/images/test4.png', title: '優惠卷4', des: '這是優惠卷～～～～', key: 4 },
                { image: '../../assets/images/test5.png', title: '優惠卷5', des: '這是優惠卷～～～～', key: 5 },
                { image: '../../assets/images/test6.png', title: '優惠卷6', des: '這是優惠卷～～～～', key: 6 },
                { image: '../../assets/images/test7.png', title: '優惠卷7', des: '這是優惠卷～～～～', key: 7 },
                { image: '../../assets/images/test8.png', title: '優惠卷8', des: '這是優惠卷～～～～', key: 8 },
                { image: '../../assets/images/test9.png', title: '優惠卷9', des: '這是優惠卷～～～～', key: 9 },
                { image: '../../assets/images/test10.png', title: '優惠卷10', des: '這是優惠卷～～～～', key: 10 }
            ],
        },
        housing: {
            name: '住',
            banner: [
                { title: 'Test' },
                { title: 'Test2' }
            ],
            items: [
                { image: '../../assets/images/test1.png', title: '優惠卷1', des: '這是優惠卷～～～～', key: 1 },
                { image: '../../assets/images/test2.png', title: '優惠卷2', des: '這是優惠卷～～～～', key: 2 },
                { image: '../../assets/images/test3.png', title: '優惠卷3', des: '這是優惠卷～～～～', key: 3 },
                { image: '../../assets/images/test4.png', title: '優惠卷4', des: '這是優惠卷～～～～', key: 4 },
                { image: '../../assets/images/test5.png', title: '優惠卷5', des: '這是優惠卷～～～～', key: 5 },
                { image: '../../assets/images/test6.png', title: '優惠卷6', des: '這是優惠卷～～～～', key: 6 },
                { image: '../../assets/images/test7.png', title: '優惠卷7', des: '這是優惠卷～～～～', key: 7 },
                { image: '../../assets/images/test8.png', title: '優惠卷8', des: '這是優惠卷～～～～', key: 8 },
                { image: '../../assets/images/test9.png', title: '優惠卷9', des: '這是優惠卷～～～～', key: 9 },
                { image: '../../assets/images/test10.png', title: '優惠卷10', des: '這是優惠卷～～～～', key: 10 }
            ],
        },
        transportation: {
            name: '行',
            banner: [
                { title: 'Test' },
                { title: 'Test2' }
            ],
            items: [
                { image: '../../assets/images/test1.png', title: '優惠卷1', des: '這是優惠卷～～～～', key: 1 },
                { image: '../../assets/images/test2.png', title: '優惠卷2', des: '這是優惠卷～～～～', key: 2 },
                { image: '../../assets/images/test3.png', title: '優惠卷3', des: '這是優惠卷～～～～', key: 3 },
                { image: '../../assets/images/test4.png', title: '優惠卷4', des: '這是優惠卷～～～～', key: 4 },
                { image: '../../assets/images/test5.png', title: '優惠卷5', des: '這是優惠卷～～～～', key: 5 },
                { image: '../../assets/images/test6.png', title: '優惠卷6', des: '這是優惠卷～～～～', key: 6 },
                { image: '../../assets/images/test7.png', title: '優惠卷7', des: '這是優惠卷～～～～', key: 7 },
                { image: '../../assets/images/test8.png', title: '優惠卷8', des: '這是優惠卷～～～～', key: 8 },
                { image: '../../assets/images/test9.png', title: '優惠卷9', des: '這是優惠卷～～～～', key: 9 },
                { image: '../../assets/images/test10.png', title: '優惠卷10', des: '這是優惠卷～～～～', key: 10 }
            ],
        },
        education: {
            name: '育',
            banner: [
                { title: 'Test' },
                { title: 'Test2' }
            ],
            items: [
                { image: '../../assets/images/test1.png', title: '優惠卷1', des: '這是優惠卷～～～～', key: 1 },
                { image: '../../assets/images/test2.png', title: '優惠卷2', des: '這是優惠卷～～～～', key: 2 },
                { image: '../../assets/images/test3.png', title: '優惠卷3', des: '這是優惠卷～～～～', key: 3 },
                { image: '../../assets/images/test4.png', title: '優惠卷4', des: '這是優惠卷～～～～', key: 4 },
                { image: '../../assets/images/test5.png', title: '優惠卷5', des: '這是優惠卷～～～～', key: 5 },
                { image: '../../assets/images/test6.png', title: '優惠卷6', des: '這是優惠卷～～～～', key: 6 },
                { image: '../../assets/images/test7.png', title: '優惠卷7', des: '這是優惠卷～～～～', key: 7 },
                { image: '../../assets/images/test8.png', title: '優惠卷8', des: '這是優惠卷～～～～', key: 8 },
                { image: '../../assets/images/test9.png', title: '優惠卷9', des: '這是優惠卷～～～～', key: 9 },
                { image: '../../assets/images/test10.png', title: '優惠卷10', des: '這是優惠卷～～～～', key: 10 }
            ],
        },
        entertainment: {
            name: '樂',
            banner: [
                { title: 'Test' },
                { title: 'Test2' }
            ],
            items: [
                { image: '../../assets/images/test1.png', title: '優惠卷1', des: '這是優惠卷～～～～', key: 1 },
                { image: '../../assets/images/test2.png', title: '優惠卷2', des: '這是優惠卷～～～～', key: 2 },
                { image: '../../assets/images/test3.png', title: '優惠卷3', des: '這是優惠卷～～～～', key: 3 },
                { image: '../../assets/images/test4.png', title: '優惠卷4', des: '這是優惠卷～～～～', key: 4 },
                { image: '../../assets/images/test5.png', title: '優惠卷5', des: '這是優惠卷～～～～', key: 5 },
                { image: '../../assets/images/test6.png', title: '優惠卷6', des: '這是優惠卷～～～～', key: 6 },
                { image: '../../assets/images/test7.png', title: '優惠卷7', des: '這是優惠卷～～～～', key: 7 },
                { image: '../../assets/images/test8.png', title: '優惠卷8', des: '這是優惠卷～～～～', key: 8 },
                { image: '../../assets/images/test9.png', title: '優惠卷9', des: '這是優惠卷～～～～', key: 9 },
                { image: '../../assets/images/test10.png', title: '優惠卷10', des: '這是優惠卷～～～～', key: 10 }
            ],
        },
    },
}

export default Home;
