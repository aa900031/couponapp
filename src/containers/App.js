import React, {Component} from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import Icon from 'react-native-vector-icons/MaterialIcons';
// 引入 Container
import Home from './Home'
import Drawer from './Drawer'
import Search from './Search'
import Shop from './Shop'
import Test from './Test'
import Product from './Product'
// 引入其他
import reducers from '../reducers';
// 初始化
const RouterWithRedux = connect()(Router);
const store = compose(
    applyMiddleware(
        thunkMiddleware,
        createLogger()
    )
)(createStore)(reducers);

class App extends Component {
    constructor(props) {
        super(props);
        this.preloadImages = this.preloadImages.bind(this);
        this.state = {};
    }
    componentDidMount() {
        this.preloadImages();
    }
    render() {
        if (!this.state.icons) return null;
        console.log(this.state.icons.menu);
        return(
            <Provider store={store}>
                <RouterWithRedux>
                    <Scene key="drawer" component={Drawer} open={false}>
                        <Scene key="main" tabs={true}>
                            <Scene key="home" component={Home} title="首頁" drawerImage={{...this.state.icons.menu, height:40, width:40}}/>
                            <Scene key="search" component={Search} hideNavBar={true} title="搜尋" drawerImage={{...this.state.icons.menu, height:40, width:40}}/>
                            <Scene key="test" component={Test} title="測試" hideNavBar={true} drawerImage={{...this.state.icons.menu, height:40, width:40}}/>
                        </Scene>
                    </Scene>
                    <Scene key="shop" component={Shop} hideNavBar={true}/>
                    <Scene key="product" component={Product} title="商品" hideNavBar={false}/>
                </RouterWithRedux>
            </Provider>
        );
    }

    async preloadImages() {
        const images = await Promise.all([
            Icon.getImageSource('menu', 30, 'black'),
        ]);
        this.setState({
            icons: {
                menu: images[ 0 ],
            }
        });
    }
}

export default App;
