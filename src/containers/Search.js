import React, {PropTypes, Component} from 'react';
import ReactNative, {View, StyleSheet} from 'react-native';

import SearchBar from '../components/Search/SearchBar.js';
import Categories from '../components/Search/Categories';

class Search extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <SearchBar/>
                <Categories/>
            </View>
        );
    }

    onSearch = () => {

    }
}

Search.propTypes = {
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    }
})

export default Search;
