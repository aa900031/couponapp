import React, {PropTypes, Component} from 'react';
import ReactNative, {View, Text, Image, ScrollView, StyleSheet, PixelRatio, TouchableOpacity} from 'react-native';
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';
import SwipeableViews from 'react-swipeable-views/lib/index.native.animated';

import Main from '../components/Shop/Main';
import Products from '../components/Shop/Products';

class Shop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabIndex: 0,
        }
    }

    render() {
        return <ScrollView style={styles.container}>
            <Image style={styles.imgHeader} source={{uri: 'http://placehold.it/350x150?text=ShopHeader'}} />
            <View style={styles.tabBar}>
                <TouchableOpacity style={[styles.tabItem, this.state.tabIndex === 0 ? styles.tabUnderLine : {}]}
                    onPress={()=>this.handleChangeIndex(0)}
                >
                    <Text style={styles.tabTitle}>首頁</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.tabItem, this.state.tabIndex === 1 ? styles.tabUnderLine : {}]}
                    onPress={()=>this.handleChangeIndex(1)}
                >
                    <Text style={styles.tabTitle}>商店</Text>
                </TouchableOpacity>
            </View>
            <SwipeableViews style={{flex: 1,}}
                index={this.state.tabIndex}
                onChangeIndex={this.handleChangeIndex}
                containerStyle={styles.slideContainer}
            >
                <Main tabLabel="首頁"/>
                <Products tabLabel="商店"/>
            </SwipeableViews>
        </ScrollView>
    }

    handleChangeIndex = (index) => {
        if (this.state.tabIndex !== index) this.setState({ tabIndex: index });
    }
}

Shop.propTypes = {
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    imgHeader: {
        // flex: 1,
        height: 210,
    },
    tabBar: {
        flexDirection: 'row',
        borderBottomWidth: (1 / PixelRatio.get()),
        borderBottomColor: 'gray',
    },
    tabItem: {
        flex: 1,
        flexDirection: 'row',
        height: 40,
        backgroundColor: 'white',
    },
    tabTitle: {
        flex: 1,
        fontSize: 16,
        textAlign: 'center',
        alignSelf: 'center',
    },
    tabUnderLine: {
        borderBottomColor: '#0277BD',
        borderBottomWidth: 3,
    },
})

export default Shop;
