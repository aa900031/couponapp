import _ from 'lodash';
import React, {Component} from 'react';
import Drawer from 'react-native-drawer';
import {Actions, DefaultRenderer} from 'react-native-router-flux';

import Content from '../components/Drawer/Content';

export default class extends Component {
    render() {
        const state = this.props.navigationState;
        const children = state.children;
        return (
            <Drawer
                ref="navigation"
                open={state.open}
                onOpen={()=>Actions.refresh({key:state.key, open: true})}
                onClose={()=>Actions.refresh({key:state.key, open: false})}
                type="overlay"
                content={<Content drawKey={state.key}/>}
                tapToClose={true}
                styles={{
                    shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3
                }}
                // openDrawerOffset={0.2}
                openDrawerOffset={(viewport) => viewport.width - 280}
                panCloseMask={0.2}
                negotiatePan={true}
                tweenHandler={(ratio) => ({
                    main: { opacity:Math.max(0.54,1-ratio) }
                    // main: { opacity:(2-ratio)/2 }
                })}
            >
                <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
            </Drawer>
        );
    }
}
