import React, { PropTypes, Component } from 'react';
import ReactNative, { View, StyleSheet, ScrollView, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import HeaderBanner from '../components/Product/HeaderBanner';

class Product extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[styles.container, {marginTop: 60,}]}>
                <ScrollView style={styles.container}>
                    <HeaderBanner data={fackData.banner} />
                    <View style={styles.header}>
                        <Text style={styles.headerTitle}>{`${fackData.name}`}</Text>
                        <View style={styles.priceContainer}>
                            <View style={styles.blockContainer}>
                                <Text style={styles.priceTitle}>{`售價`}</Text>
                                <Text style={styles.priceValue}>{`NT$ ${fackData.price}`}</Text>
                            </View>
                            <View style={styles.blockContainer}>
                                <Text style={styles.priceTitle}>{`數量`}</Text>
                                <Text style={styles.priceAmount}>{`${fackData.count}/個`}</Text>
                            </View>
                        </View>
                    </View>
                    <View sytle={styles.content}>
                        <Text style={styles.contentText}>
                            {fackData.disc}
                        </Text>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <TouchableOpacity onPress={() => {}} style={styles.btnFavorite}>
                        <Icon name='favorite-border' size={30}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {}} style={styles.btnAddCart}>
                        <Text style={styles.textAddCart}>立即購買</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {}} style={styles.btnBuy}>
                        <Text style={styles.textBuy}>加入購物車</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

Product.propTypes = {
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    header: {
        padding: 10,
        backgroundColor: 'white',
    },
    headerTitle: {
        fontSize: 20,
        color: 'black',
        height: 50,
    },
    priceContainer: {
        flexDirection: 'column',
        paddingTop: 6,
        paddingBottom: 6,
    },
    blockContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray',
        padding: 10,
    },
    priceTitle: {
        // flex: 1,
        fontSize: 18,
        color: '#607D8B',
        alignSelf: 'center',
    },
    priceValue: {
        // flex: 2,
        fontSize: 20,
        color: 'red',
        alignSelf: 'center',
    },
    priceAmount: {
        // flex: 2,
        fontSize: 18,
        color: 'red',
        alignSelf: 'center',
    },
    content: {
        flex: 1,

    },
    contentText: {
        fontSize: 15,
        paddingRight: 10,
        paddingLeft: 10,
    },
    footerContainer: {
        position: 'relative',
        backgroundColor: '#FAFAFA',
        flexDirection: 'row',
        bottom: 0,
        height: 40,
        borderTopColor: '#b2b2b2',
        borderTopWidth: 1,
    },
    btnFavorite: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnAddCart: {
        flex: 3,
        backgroundColor: '#FFA726',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textAddCart: {
        color: 'white',
    },
    btnBuy: {
        flex: 3,
        backgroundColor: '#F4511E',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textBuy: {
        color: 'white',
    },
})

const fackData = {
    name: "TestProduct",
    price: 1000,
    disc: "商品內容 測試測試測試測試  商品內容 測試測試測試測試  商品內容 測試測試測試測試  商品內容 測試測試測試測試  商品內容 測試測試測試測試",
    count: 100,
    banner: [
        { title: 'Test' },
        { title: 'Test' }
    ]
}

export default Product;
